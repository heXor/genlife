from enum import Enum, IntEnum
from random import *
from array import *
from copy import *
import hashlib


def rr(range_min, range_max: int):
    """ randrange """
    return randrange(range_min, range_max + 1)


class NEW_PixiGeneticCmd(IntEnum):
    no_action = 0
    move = 1
    rotate_rnd = 2
    rot_left = 3
    rot_rigth = 4
    stay = 5
    reset = 6


class NEW_PixiGeneticCnn(IntEnum):
    """ CoNditioN """
    always     = 0
    if_rnd     = 1

class PixiGeneticCmd(IntEnum):
    nop = 0
    move = 1
    rotate_rnd = 2
    rotate_l = 3
    rotate_r = 4
    stay = 5
    reset = 6
    wait = 7  # wait one "world iteration" by not reset step
    # todo: для поддержки нужны двух байтовые команды - чтобы у условия можно было задавать число сравнения.
    #spawn    = 8 # родить
    selfkill = 13 # убить себя
    if_rnd_1_30 = 129
    if_rnd_50_50 = 130
    if_eat_f1 = 131
    if_eat_f2 = 132
    if_eat_f3 = 133
    if_eat_l = 134
    if_eat_r = 135
    if_wall = 136
    if_empty = 137
    if_kill = 138
    # нужен особый режим мутации. при рождении у ребенка константа сравнения всегда подкручивается в плюс или минус.
    # типа разброс констант (как у всех организмов).
    # грубая имитация
    if_life_less = 140
    if_life_great = 141
    #if_tick_n


class ItemType(IntEnum):
    empty = 0
    eat = 1
    pixi = 2
    wall = 3
    kill = 5


class PixiGenetic:
    __slots__ = 'gen', 'wait_step', 'action', 'eye'

    def __init__(self):
        self.eye = [0, 0, 0, 0, 0]  # вперед1, вперед2, вперед3, бок-лево, бок-право
        self.wait_step = -1
        self.gen = array('B')
        self.action = 0

    def getkey(self, keylen=8):
        return hashlib.md5(','.join(map(lambda x: str(x), self.gen)).encode()).hexdigest()[:keylen]

    @staticmethod
    def is_cmd_cond(cmd):
        """ command-condition / RU: команда-условие """
        return cmd >= 128

    @staticmethod
    def cmd_len(cmd):
        """ command len / RU: узнать размер команды """
        if cmd == PixiGeneticCmd.if_life_less or cmd == PixiGeneticCmd.if_life_great:
          return 2
        return 1

    def run(self):
        if len(self.gen) == 0:
            self.action = int(PixiGeneticCmd.selfkill)
            return

        if self.wait_step != -1:
            step = self.wait_step
            self.wait_step = -1
        else:
            step = 0

        while True:

            if step >= len(self.gen):
                self.action = 0
                return

            cmd = self.gen[step]

            skip = True
            if PixiGenetic.is_cmd_cond(cmd):
                # если это "условие" тогда:

                # если это "eat зрение"
                if int(PixiGeneticCmd.if_eat_f1) <= cmd <= int(PixiGeneticCmd.if_eat_r):
                    if self.eye[cmd - int(PixiGeneticCmd.if_eat_f1)] == int(ItemType.eat):
                        skip = False
                elif cmd == int(PixiGeneticCmd.if_wall):
                    if self.eye[0] == int(ItemType.wall):
                        skip = False
                elif cmd == int(PixiGeneticCmd.if_kill):
                    if self.eye[0] == int(ItemType.kill):
                        skip = False
                elif cmd == int(PixiGeneticCmd.if_empty):
                    if self.eye[0] == int(ItemType.empty):
                        skip = False
                elif cmd == int(PixiGeneticCmd.if_rnd_1_30):
                    if rr(1, 30) == 1:  # шанс 1 к 30 что команда выполнится
                        skip = False
                elif cmd == int(PixiGeneticCmd.if_rnd_50_50):
                    if rr(1, 2) == 1:  # 50/50 что команда выполнится
                        skip = False

                if skip:
                    # пропускаем последовательность "команд-условий"
                    while True:
                        step += self.cmd_len(cmd)  # пропускаем это условие
                        if step >= len(self.gen):
                            break
                        else:
                            if PixiGenetic.is_cmd_cond(self.gen[step]):
                                # если за условием идет еще одно условие то его пропускаем тоже
                                continue
                            else:
                                step += self.cmd_len(self.gen[step])  # пропускаем следующую команду (перепрыгиваем)
                                break
                else:
                    step += 1  # пропускаем эту команду и забираем следующую команду

                # если следующий шаг это снова "команда-условие" (или NOP), тогда повторяем цикл
                if step < len(self.gen) and (
                        PixiGenetic.is_cmd_cond(self.gen[step]) or self.gen[step] == int(PixiGeneticCmd.nop)):
                    continue
                else:
                    break
            else:
                # если это не "команда-условие" тогда всегда выходим из цикла
                break

            # end while

        if step >= len(self.gen):
            self.action = PixiGeneticCmd.selfkill.value
            return
        else:
            # передаем команду в действия - будет выполнено действие
            self.action = self.gen[step]

        # внутринние действия генома
        if self.action == PixiGeneticCmd.stay.value:
            self.action = 0
        elif self.action == PixiGeneticCmd.reset.value:
            self.action = 0
        elif self.action == PixiGeneticCmd.wait.value:
            self.wait_step = step
            self.action = 0
        elif self.action == PixiGeneticCmd.nop.value:
            self.action = 0

    @staticmethod
    def cmd_to_str(cmd):
        return str(PixiGeneticCmd(cmd).name)

    def decompile(self):
        s = []
        for i in self.gen:
            s.append(self.cmd_to_str(i))
        return s

    def compile(self, strlist):
        del self.gen[:]
        for s in strlist:
            self.gen.append(PixiGeneticCmd[s].value)

    def optimize(self):
        if len(self.gen) == 0:
            return []

        step = 0
        skip = False
        while True:
            cmd = self.gen[step]

            if PixiGenetic.is_cmd_cond(cmd) or cmd == int(PixiGeneticCmd.nop):
                # пропускаем следующую команду (перепрыгиваем)
                skip = True
                step += 1
            else:
                if skip:
                    skip = False
                    step += 1
                else:
                    # если это обычная "команда" и она не покрыта условием (без условная команда)
                    # тогда выходим из цикла, программа закончилась.
                    break

            if step >= len(self.gen):
                break

        self.gen = self.gen[:step + 1]


class Pixi:
    const_rotate_pack = [-1, 1]
    pixi_max_age = 10000
    dir_step = ((0, -1), (1, 0), (0, 1), (-1, 0))
    world_size_x: int = 0
    world_size_y: int = 0
    max_stay_count: int = 10

    def __init__(self, x: int, y: int):
        self.gen: PixiGenetic = None
        self.uid: int = 0
        self.pos: (int, int) = (x, y)
        self.pos_old: (int, int) = self.pos
        self.stay_count: int = 0
        self.action: int = 1
        self.dir: int = 1  # 0-^, 1->, 2-v, 3-<.
        self.life: int = 1000
        self.age: int = 0
        self.dead: bool = True
        self.must_die: bool = False
        self.spawn: int = 0  # spawn new pixi count

    def get_moved_pos(self):
        return ((self.pos[0] + self.dir_step[self.dir][0]) % self.world_size_x,
                (self.pos[1] + self.dir_step[self.dir][1]) % self.world_size_y)

    def create_gen(self):
        self.gen = PixiGenetic()

    def run(self):
        if self.pos == self.pos_old:
            self.stay_count += 1
            if self.stay_count > self.max_stay_count:
                # "Wer rastet, der rostet"
                self.must_die = True
                return
        else:
            self.pos_old = self.pos
            self.stay_count = 0

        if self.life < 0 or self.age > self.pixi_max_age:
            self.must_die = True
            return

        self.gen.run()
        self.action = self.gen.action

        self.age = self.age + 1
        self.life = self.life - 1

        if self.action == int(PixiGeneticCmd.selfkill):
            self.must_die = True
        elif self.action == PixiGeneticCmd.rotate_rnd.value:
            self.rotate(self.const_rotate_pack[rr(0, 1)])
            self.life = self.life - 4
        elif self.action == PixiGeneticCmd.rotate_l.value:
            self.rotate(-1)
            self.life = self.life - 4
        elif self.action == PixiGeneticCmd.rotate_r.value:
            self.rotate(1)
            self.life = self.life - 4
        elif self.action == PixiGeneticCmd.move.value:
            self.life = self.life - 5
        elif self.action == 0:
            # not move.
            pass
        else:
            self.life = self.life - 100  # - ШТРАФ


    def rotate(self, vector):
        self.dir = (self.dir + vector) % 4
