from pixi_population import *
from math import *

##############################################
#
# 0-^    012    0: -1,-1  1: 0,-1  2: +1,-1
#        345    3: -1,0   4: 0,0   5: +1,0
#        678    6: -1,+1  7: 0,+1  8: +1,+1
#
# 1->    630
#        741
#        852
#
# 2-v    876
#        543
#        210
#
# 3-<    258
#        147
#        036
#
st = (
    (-1, -1), (0, -1), (+1, -1),
    (-1, 0), (0, 0), (+1, 0),
    (-1, +1), (0, +1), (+1, +1)
)

#todo: move to WorldAreaAbstract
# я немного запутался, но вроде норм
dirs = (
    (st[0], st[1], st[2], st[3], st[4], st[5], st[6], st[7], st[8]),
    (st[2], st[5], st[8], st[1], st[4], st[7], st[0], st[3], st[6]),
    (st[8], st[7], st[6], st[5], st[4], st[3], st[2], st[1], st[0]),
    (st[6], st[3], st[0], st[7], st[4], st[1], st[8], st[5], st[2]),
)
del st



class WorldAreaAbstract:
    __slots__ = 'sx', 'sy'

    def __init__(self, world_size_x: int, world_size_y: int):
        self.sx = world_size_x  # size x
        self.sy = world_size_y  # size y

    def get(self, x: int, y: int):
        return 0

    def set(self, x: int, y: int, c: int):
        return



class WorldAreaNative(WorldAreaAbstract):

    def __init__(self, world_size_x: int, world_size_y: int):
        super().__init__(world_size_x, world_size_y)
        self.world_array = array('B') * world_size_x * world_size_y

    def get(self, x: int, y: int):
        return self.world_array[(x // self.sx) + (y % self.sy)]

    def set(self, x: int, y: int, c: int):
        self.world_array[(x // self.sx) + (y % self.sy)] = c


class WorldAreaPG(WorldAreaAbstract):
    __slots__ = 'tx'

    def __init__(self, world_size_x: int, world_size_y: int, pygamelib):
        super().__init__(world_size_x, world_size_y)
        tx = pygamelib.Surface((world_size_x, world_size_y), flags=0, depth=8)
        tx.fill(0)
        self.tx = tx

    def get(self, x: int, y: int):
        return self.tx.get_at_mapped((x % self.sx, y % self.sy))

    def set(self, x: int, y: int, c: int):
        self.tx.set_at((x, y), c)


class WorldEngineBase:

    def __init__(self, config):
        cfg = config
        self.cfg = config

        self.area: WorldAreaAbstract = None

        self.pixis = PixiPopulation(cfg, self)

        self.green_gen_speed = cfg.world_size_x * cfg.world_size_y / cfg.green
        self.wall_count = cfg.world_size_x * cfg.world_size_y / cfg.walls
        self.firewall_count = cfg.world_size_x * cfg.world_size_y / cfg.firewalls
        self.green_spawn_count = 0
        self.green_spawn_count_prev = 0

    def run(self, area):
        # todo: move to PixiPop.

        self.green_spawn_count = self.green_spawn_count + self.green_gen_speed
        if int(self.green_spawn_count - self.green_spawn_count_prev) > 0:
            for x in range(int(self.green_spawn_count - self.green_spawn_count_prev)):
                self.spawn_green((rr(1, self.cfg.world_size_x), rr(1, self.cfg.world_size_y)), 5)
            self.green_spawn_count_prev = self.green_spawn_count
            if self.green_spawn_count > 1000000:
                self.green_spawn_count = 0
                self.green_spawn_count_prev = 0

        dead_list = []
        birth_list = []
        # iterate all living pixi
        for k, p in self.pixis.life_pixs.items():
            p.gen.eye[0] = area.get(p.pos[0] + dirs[p.dir][1][0] * 1, p.pos[1] + dirs[p.dir][1][1] * 1)  # f1
            p.gen.eye[1] = area.get(p.pos[0] + dirs[p.dir][1][0] * 2, p.pos[1] + dirs[p.dir][1][1] * 2)  # f2
            p.gen.eye[2] = area.get(p.pos[0] + dirs[p.dir][1][0] * 3, p.pos[1] + dirs[p.dir][1][1] * 3)  # f3
            p.gen.eye[3] = area.get(p.pos[0] + dirs[p.dir][3][0], p.pos[1] + dirs[p.dir][3][1])  # l
            p.gen.eye[4] = area.get(p.pos[0] + dirs[p.dir][5][0], p.pos[1] + dirs[p.dir][5][1])  # r
            p.run()
            if p.must_die:
                dead_list.append(p)
                continue
            # get new pos
            pos = p.get_moved_pos()
            #todo: OLD: pos = self.movepos(p.pos, p.dir, area)

            if area.get(pos[0], pos[1]) == 1:
                # eat (and not move)
                area.set(pos[0], pos[1], 0)
                p.life = p.life + 100
                if p.life > 4000:
                    # birth new!
                    newp = self.pixis.birth(p)
                    if newp is not None:
                        birth_list.append(newp)
                        if rr(1, 100) <= self.cfg.mutation_percent and (
                                p.spawn > 1 and p.age > self.cfg.pixi_max_age // 4):
                            # mutation only if this second child
                            # first child always is clear of mutation.
                            # if pixi is not young  - then mutation.
                            self.pixis.mutation(newp)
            elif p.action == 1:
                # move
                if area.get(pos[0], pos[1]) == 0:
                    p.pos = pos
                elif area.get(pos[0], pos[1]) == 5:
                    p.dead = True
                    p.must_die = True
                    dead_list.append(p)
                    continue

        # remove dead pixi
        for p in dead_list:
            self.pixis.kill(p)
        # create new pixi
        for p in birth_list:
            self.pixis.life_pixs.update({p.uid: p})

    def init_world(self, area):
        # generate kills hole
        for x in range(int(self.firewall_count)):
            # pos = (rr(1,self.cfg.world_size_x), rr(1,self.cfg.world_size_y))
            pos = (rr(1, self.cfg.world_size_x), int(log(rr(1, self.cfg.world_size_y)) * 200) - 500)
            for i in range(5):
                area.set(pos[0], pos[1] + i, 5)

        # generate walls
        for x in range(int(self.wall_count)):
            pos = (rr(1, self.cfg.world_size_x), rr(1, self.cfg.world_size_y))
            for i in range(5):
                area.set(pos[0] + i, pos[1], 3)

        # first spawn
        for x in range(int(self.green_gen_speed * 100)):
            self.spawn_green((rr(1, self.cfg.world_size_x), rr(1, self.cfg.world_size_y)), 3)

    def spawn_green(self, pos, size):
        for x in range(-size, size):
            for y in range(-size, size):
                if (0 <= (pos[0] + x) <= self.area.sx) and (0 <= (pos[1] + y) <= self.area.sy):
                    if self.area.get(pos[0] + x, pos[1] + y) == 0:
                        self.area.set(pos[0] + x, pos[1] + y, 1)

