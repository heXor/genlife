from collections import OrderedDict
import oyaml as yaml  # pip3 install pyyaml

from pixi import *
from world import *
from world_config import *

class PixiPopulation:

    def __init__(self, config: WorldConfig, world):
        self.area: WorldAreaAbstract = None
        self.world: WorldEngineBase = world
        self.cfg: WorldConfig = config
        self.mutation_list = list(map(lambda x: x.value, list(PixiGeneticCmd)))
        self.rotate_pack = [-1, 1]
        self.dir_step = ((0, -1), (1, 0), (0, 1), (-1, 0))

        Pixi.world_size_x: int = config.world_size_x
        Pixi.world_size_y: int = config.world_size_y

        self.birth_count = 0
        self.mutation_count = 0
        self.dead_pixs = []  # dead pixi
        self.life_pixs:dict(Pixi) = {}  # living pixi
        self.pixi_pool = []  # pixi main pool

    def setup(self, area):
        self.area = area

    def run(self):
        raise Exception('WIP!!!' + str(self))
        pass

    def total_init(self):
        for i in range(1, self.cfg.pixi_max_count + 1):
            p = Pixi(rr(1, self.area.sx), rr(1, self.area.sy))
            p.create_gen()  # todo:UPD del 0
            p.uid = i
            self.pixi_pool.append(p)
            # todo: init all
            # if rr(1,10)==1:
            p.life = randrange(1000, 2002)
            self.life_pixs.update({i: p})

            p.gen.gen = copy([
                PixiGeneticCmd.if_kill.value,
                PixiGeneticCmd.rotate_l.value,
                PixiGeneticCmd.move.value,
                PixiGeneticCmd.reset.value])

            # for _ in range(10):
            #  self.mutation(p)
            # else:
            #  p.life = -1
            #  self.dead_pixs.append(p)
        self.mutation_count = 0

    def kill(self, target: Pixi):
        target.must_die = False
        target.dead = True
        self.dead_pixs.append(target)
        # print(target.gen.gen) #DBG!!!
        del self.life_pixs[target.uid]
        # spawn green
        if target.life > 500 and target.age > Pixi.pixi_max_age:
            self.world.spawn_green(target.pos, 1)
        elif target.age > Pixi.pixi_max_age:
            self.world.spawn_green(target.pos, 1)
        else:
            self.world.area.set(target.pos[0], target.pos[1], 1)

    def mutation(self, target: Pixi):

        self.mutation_count = self.mutation_count + 1

        if rr(1, 2) == 1:
            newcmd = 0  # nop
        else:
            newcmd = self.mutation_list[rr(0, len(self.mutation_list) - 1)]
            # if is "condition" then try again
            # if PixiGenetic.is_cmd_cond(newcmd):
            #  newcmd = self.mutation_list[rr(0,len(self.mutation_list)-1)]
            #  if PixiGenetic.is_cmd_cond(newcmd):
            #    newcmd = self.mutation_list[rr(0,len(self.mutation_list)-1)]

        # увеличить размер
        if len(target.gen.gen) < 8:
            target.gen.gen.append(newcmd)
        else:
            if rr(1, 100) < 10 and len(target.gen.gen) < self.cfg.gen_max_size:
                target.gen.gen.insert(rr(0, len(target.gen.gen) - 1), newcmd)

        # уменьшить размер
        if rr(1, 100) < 10 and len(target.gen.gen) > 0:
            del target.gen.gen[rr(0, len(target.gen.gen) - 1)]

        # замена всех nop
        if rr(1, 100) < 50 and len(target.gen.gen) > 0:
            target.gen.gen = list(map(lambda x: newcmd if x == 0 else x, target.gen.gen))
        else:
            # ИЛИ заменить случайную команду
            if len(target.gen.gen) > 0:
                target.gen.gen[rr(0, len(target.gen.gen) - 1)] = newcmd

        # починить
        if len(target.gen.gen) > 0:
            last = target.gen.gen[len(target.gen.gen) - 1]
            if PixiGenetic.is_cmd_cond(last):
                # если последняя команда это условие - тогда заменяем на reset
                target.gen.gen[len(target.gen.gen) - 1] = int(PixiGeneticCmd.nop)

    def total_optimize(self):
        """ enum all pixi and optimize gen code """
        for _, p in self.life_pixs.items():
            p.gen.optimize()

    def total_mutation(self):
        for _, p in self.life_pixs.items():
            self.mutation(p)

    def birth(self, source: Pixi):
        if len(self.dead_pixs) > 0:
            self.birth_count = self.birth_count + 1
            source.spawn += 1
            newpixi = self.dead_pixs.pop()
            newpixi.gen = deepcopy(source.gen)
            newpixi.pos = source.pos
            newpixi.age = 0
            newpixi.spawn = 0
            newpixi.life = int(source.life * 0.2)
            source.life = int(source.life * 0.7)
            newpixi.dead = False
            newpixi.rotate(newpixi.dir + 2 + rr(-1, 1))
            # print(','.join(newpixi.gen.decompile())) #DBG!!!
            return newpixi
        else:
            return None

    def all_kill(self):
        self.life_pixs.clear()
        self.dead_pixs.clear()
        for p in self.pixi_pool:
            p.dead = True
            p.life = 0
            p.age = 1000000
            self.dead_pixs.append(p)

    @staticmethod
    def genepool_sort(gens):
        gens = OrderedDict(sorted(gens.items(), key=lambda kv: int(kv[1]['count']), reverse=True))
        return gens

    @staticmethod
    def genepool_trim(gens, min_count):
        if len(gens) < 2:
            return gens
        gens2 = copy(gens)
        for key, node in gens.items():
            if node['count'] < min_count:
                del gens2[key]
        return gens2

    def genepool_save(self, gens):
        # create data
        with open(self.cfg.genepool_filename, 'w', encoding='utf-8') as f:
            yaml.dump(gens, f, default_flow_style=False, allow_unicode=True)

    def genepool_get(self):
        gens = OrderedDict()

        # collect all uniqe gene
        for k, p in self.life_pixs.items():
            key = p.gen.getkey()
            if key in gens:
                gens[key]['count'] += 1
            else:
                gens.update({key: {
                    'count': 1,
                    'program': p.gen.decompile()
                }})
                gens.move_to_end(key)
        return gens

    @staticmethod
    def genepool_optimize(gens):
        gens2 = OrderedDict()
        g = PixiGenetic()

        # enum all gen, make optimization and collect to new array
        for _, node in gens.items():
            g.compile(node['program'])
            g.optimize()
            key = g.getkey()
            if key in gens2:
                gens2[key]['count'] += int(node['count'])
            else:
                gens2.update({key: {
                    'count': int(node['count']),
                    'program': g.decompile()
                }})
                gens2.move_to_end(key)
        return gens2

    def genepool_load(self):
        with open(self.cfg.genepool_filename, 'r', encoding='utf8') as stream:
            data = yaml.load(stream)
        gens = OrderedDict()
        for key, node in data.items():
            gens.update({key: {
                'count': int(node['count']),
                'program': node['program']
            }})
            gens.move_to_end(key)
        return gens

    def genepool_apply(self, gens, percent=100):
        # теряю распределение - все гены размазывает равномерно, но пока это не самая большая проблема
        # ... genshift = len(self.pixi_pool) / len(gens)
        # i = 0
        if len(gens) == 0:
            return

        for k, g in gens.items():
            spawn_count = int(g['count'] * (1 / 100 * percent))
            i = 0
            for p in self.pixi_pool:
                if p.dead:
                    i += 1
                    if i > spawn_count:
                        break
                    p.gen.compile(copy(g['program']))  # copy необязательно, но на всякий случай...
                    p.dead = False
                    p.age = 0
                    p.life = randrange(1000, 1500)
                    self.dead_pixs.remove(p)
                    self.life_pixs.update({p.uid: p})

