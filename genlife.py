﻿"""

pygame.Surface((width, height), flags=0, depth=0, masks=None) -> Surface

pygame.Surface.get_at 	— 	get the color value at a single pixel
pygame.Surface.set_at 	— 	set the color value for a single pixel

pygame.Surface.get_at_mapped((x, y)) -> Color 	— 	get the mapped color value at a single pixel
pygame.Surface.get_palette -> [RGB, RGB, RGB, ...] 	— 	get the color index palette for an 8-bit Surface
pygame.Surface.get_palette_at -> RGB 	— 	get the color for a single entry in a palette
pygame.Surface.set_palette([RGB, RGB, RGB, ...]) 	— 	set the color palette for an 8-bit Surface
pygame.Surface.set_palette_at(index, RGB) 	— 	set the color for a single index in an 8-bit Surface palette
Surface.get_bytesize()

"""

from math import *
from copy import *
import sys
from random import randrange
import pygame as pg  # pip install pygame
from enum import Enum, IntEnum
from pprint import pprint
import os
# todo: add: from docopt import docopt

from world import *
from world_config import *
from pixi import *
from pixi_population import *



def rr(range_min, range_max: int):
    """ randrange """
    return randrange(range_min, range_max + 1)



class WorldEnginePG(WorldEngineBase):

    def __init__(self, config):
        super().__init__(config)

        cfg = config

        self.done = False
        pg.init()  # todo:???
        self.font = pg.font.Font(None, 30)
        self.clock = pg.time.Clock()
        self.screen = pg.display.set_mode((cfg.scr_width, cfg.scr_height), pg.HWSURFACE | pg.DOUBLEBUF | pg.RESIZABLE)
        pg.display.set_caption('Genetic Algorithm System')

        # title and icon
        wnd_icon = pg.Surface((32, 32), flags=0, depth=24)
        pg.draw.rect(wnd_icon, (0, 0, 0), (0, 0, 32, 32), 0)
        pg.draw.rect(wnd_icon, (0, 127, 0), (12, 12, 16, 16), 0)
        pg.draw.rect(wnd_icon, (255, 255, 255), (6, 7, 3, 3), 0)
        pg.draw.rect(wnd_icon, (255, 255, 255), (24, 17, 3, 3), 0)
        pg.draw.rect(wnd_icon, (255, 255, 255), (10, 20, 3, 3), 0)
        pg.draw.rect(wnd_icon, (127, 127, 127), (1, 1, 31, 31), 3)
        pg.display.set_icon(wnd_icon)

    def init_area(self):
        self.area = WorldAreaPG(self.cfg.world_size_x, self.cfg.world_size_y, pg)
        tx = self.area.tx
        tx.set_palette_at(0, (0, 0, 0))  # empty
        tx.set_palette_at(1, (0, 127, 0))  # green
        tx.set_palette_at(2, self.cfg.color_pixi)  # pixi
        tx.set_palette_at(3, (255, 0, 255))  # wall
        tx.set_palette_at(4, (127, 127, 0))  # ---
        tx.set_palette_at(5, (255, 0, 0))  # kill hole
        return self.area

    def on_key_pressed(self, key, mod):
        if key == pg.K_ESCAPE:
            self.done = True
        elif key == pg.K_PLUS or key == pg.K_KP_PLUS:
            self.green_gen_speed += self.green_gen_speed / 10
        elif key == pg.K_MINUS or key == pg.K_KP_MINUS:
            self.green_gen_speed -= self.green_gen_speed / 10
        elif key == pg.K_s:
            if mod & pg.KMOD_SHIFT:
                self.save_world(optimize=False)
            else:
                self.save_world(trim=True)
            print('World saved.')
        elif key == pg.K_o:
            self.pixis.total_optimize()
            print('Gen optimized.')
        elif key == pg.K_m:
            self.pixis.total_mutation()
            print('Mutation!')

    def save_world(self, optimize: bool = True, trim: bool = True):
        if len(self.pixis.life_pixs) > 0:
            gens = self.pixis.genepool_get()
            if optimize:
                gens = self.pixis.genepool_optimize(gens)
            if trim:
                # get first item - is much faster than list(gens.items())[0]
                first_node = next(iter(gens.items()))[1]
                trim_min = int(first_node['count']) // 100  # 1%
                gens = self.pixis.genepool_trim(gens, trim_min)
            gens = self.pixis.genepool_sort(gens)
            self.pixis.genepool_save(gens)
            return True
        else:
            return False

    def main_loop(self):
        cfg = self.cfg
        self.done = False

        while not self.done:
            for event in pg.event.get():
                if event.type == pg.KEYDOWN:
                    self.on_key_pressed(event.key, event.mod)
                elif event.type == pg.VIDEORESIZE:
                    self.screen = pg.display.set_mode((event.w, event.h), pg.RESIZABLE)
                elif event.type == pg.QUIT:
                    self.done = True

            # call main calc
            self.run(self.area)

            # debug!!!
            # if self.pixi_pool[0].dead==False:
            #   print('st:', self.pixi_pool[0].gen.step,
            #     'act:', self.pixi_pool[0].gen.action,
            #     'see:', self.pixi_pool[0].gen.eye, 'gen:',
            #     self.pixi_pool[0].gen.gen, 'lv:', self.pixi_pool[0].life)

            self.screen.fill(self.cfg.color_back)
            fps = self.font.render(str(int(self.clock.get_fps())), True, cfg.color_text)
            self.clock.tick(60)

            self.screen.blit(self.area.tx, (0, 0), (0, 0, cfg.world_size_x - 1, cfg.world_size_y - 1))

            for k, p in self.pixis.life_pixs.items():
                self.render_pixi(p)

            self.screen.blit(fps, (cfg.world_size_x, 5))

            self.screen.blit(
                self.font.render(str(self.pixis.pixi_pool[0].life), True, cfg.color_text),
                (cfg.world_size_x, 30))
            self.screen.blit(
                self.font.render('b.c.=' + str(self.pixis.birth_count), True, cfg.color_text),
                (cfg.world_size_x, 50))
            self.screen.blit(
                self.font.render('m.=' + str(self.pixis.mutation_count), True, cfg.color_text),
                (cfg.world_size_x, 70))
            self.screen.blit(
                self.font.render('cnt=' + str(len(self.pixis.life_pixs)), True, cfg.color_text),
                (cfg.world_size_x, 90))
            self.screen.blit(
                self.font.render('gr.=' + str(self.green_gen_speed), True, cfg.color_text),
                (cfg.world_size_x, 110))

            pg.display.flip()

            # end while.

    def update_area(self):
        pass

    def render_pixi(self, p: Pixi):
        self.screen.set_at(p.pos, self.cfg.color_pixi)
        self.screen.set_at((p.pos[0] + dirs[p.dir][6][0], p.pos[1] + dirs[p.dir][6][1]), self.cfg.color_pixi)
        self.screen.set_at((p.pos[0] + dirs[p.dir][8][0], p.pos[1] + dirs[p.dir][8][1]), self.cfg.color_pixi)
        # 0-^, 1->, 2-v, 3-<


#        if p.dir==0 or p.dir==3: # правый нижний
#          self.screen.set_at((p.pos[0]+1, p.pos[1]+1), self.cfg.color_pixi)
#        if p.dir==2 or p.dir==1: # левый верхний
#          self.screen.set_at((p.pos[0]-1, p.pos[1]-1), self.cfg.color_pixi)
#        if p.dir==0 or p.dir==1: # левый нижний
#          self.screen.set_at((p.pos[0]-1, p.pos[1]+1), self.cfg.color_pixi)
#        if p.dir==3 or p.dir==2: # правый верхний
#          self.screen.set_at((p.pos[0]+1, p.pos[1]-1), self.cfg.color_pixi)


def main():
    print('init engine')
    w = WorldEnginePG(WorldConfig())
    print('init area')
    area = w.init_area()
    w.pixis.setup(area)
    print('init world')
    w.init_world(area)
    for x in range(100):
        w.area.set(1, x, 5)
    print('init pixi')
    w.pixis.total_init()
    print('init ok.')
    if os.path.isfile(w.cfg.genepool_filename):
        w.pixis.all_kill()
        tmp = w.pixis.genepool_load()
        w.pixis.genepool_apply(tmp, w.cfg.load_genepool_percent)
        print('load ok')
    w.main_loop()
    pg.quit()
    print('main loop end')
    if w.save_world():
        print('save ok')
    else:
        print('not save - all dead')


# t = array('B', [1,2,3])
# t.append(4)
# print(t)
# del t[:]
# print(t)

'''
g = PixiGenetic()
g.compile("""if_wall
rotate_rnd
if_eat_l
rotate_l
if_empty
move
if_eat_r
rotate_r
if_eat_l
rotate_l
reset
reset
reset""".split('\n'))
print(g.decompile())
g.optimize()
print(g.decompile())
exit()
'''

if __name__ == "__main__":
    main()
