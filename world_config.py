class WorldConfig:
    def __init__(self):

        mode = 12

        self.gen_max_size = 32
        self.gen_max_num = 13
        self.pixi_max_age = 500
        self.green = 50000  # 50-many, 5000-low
        self.walls = 100  # 50-many, 5000-low
        self.firewalls = self.walls // 2
        self.load_genepool_percent = 100
        self.mutation_percent = 100

        # world:
        if mode == 1:
            self.world_size_x, self.world_size_y = 1024, 1024
            # self.pixi_max_count = 5000
            self.pixi_max_count = 50000
        elif mode == 2:
            self.world_size_x, self.world_size_y = 256, 256
            self.pixi_max_count = 500
        elif mode == 3:
            self.world_size_x, self.world_size_y = 128, 128
            self.pixi_max_count = 50
        elif mode == 4:
            self.green = 100000000  # 50-many, 5000-low
            self.walls = 100000000  # 50-many, 5000-low
            self.firewalls = 100000000  # 50-many, 5000-low
            self.load_genepool_percent = 100
            self.world_size_x, self.world_size_y = 64, 64
            self.pixi_max_count = 1
        elif mode == 12:
            self.world_size_x, self.world_size_y = 256, 256
            self.pixi_max_count = 16_384
        elif mode == 13:
            self.world_size_x, self.world_size_y = 128, 128
            self.pixi_max_count = 4_096

        # graphic:
        self.scr_width = 128 + 100
        self.scr_height = 128

        # colors:
        self.color_text = (255, 255, 255, 255)
        self.color_back = (0, 0, 0, 255)
        self.color_pixi = (255, 255, 255, 255)

        self.genepool_filename = '1_gen.txt'
